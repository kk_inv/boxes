<?php 

include('model/Package.class.php');
include('model/Blister.class.php');
include('model/Bag.class.php');
include('model/Box.class.php');
include('model/Pack.class.php');
include('model/Product.class.php');


$product45 = new Product(45, new Blister(12,5));
$product27 = new Product(27, new Box(6,7));
$product37 = new Product(37, new Bag(8,9.9));
$product25 = new Product(25, new Box(12,2,['decorated']));
$product21 = new Product(21, new Box(12.6, 6.5, ['decorated','ribbon']));


$pack1 = new Pack();

$pack1->addProduct($product45);
$pack1->addProduct($product45);
$pack1->addProduct($product27);
$pack1->addProduct($product37);
$pack1->addProduct($product25);
$pack1->addProduct($product21);

var_dump($pack1);

$pack1->removeProduct($product45);
$pack1->removeProduct($product45);


var_dump($pack1);
