<?php 


class Bag implements Package
{	
	private $weight;
	private $maxLength;
	
	public function __construct($weight = 0, $maxLength = 0)
	{
		$this->weight = $weight;
		$this->maxLength = $maxLength;
	}
	
    public function getWeight()
    {
    	return $weight;
    }
    
    public function getMaxLength()
    {
    	return $maxLength;
    }
}
