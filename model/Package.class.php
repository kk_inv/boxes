<?php 

interface Package
{

    public function getWeight();
    
    public function getMaxLength();
}
