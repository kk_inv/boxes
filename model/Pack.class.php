<?php 



class Pack
{
    private $products = array();
    
    public function __construct()
    {
    }
    
    public function addProduct(Product $product)
    {
        if (isset($this->products[$product->getId()])) {
            echo "This product is already in the pack\n\n";
            return;
        }
        $this->products[$product->getId()] = $product;
    }
    
    public function removeProduct(Product $product)
    {
        if (!isset($this->products[$product->getId()])) {
            echo "This product is not packed here\n\n";
            return;
        }
        
        unset($this->products[$product->getId()]);
    }
}
