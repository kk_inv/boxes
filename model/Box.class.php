<?php 


class Box implements Package
{
	private $weight;
	private $maxLength;
    private $options;
    

    public function __construct($weight = 0, $maxLength = 0, $options = [])
    {
    	$this->options = $options;
    	$this->weight = $weight;
    	$this->maxLength = $maxLength;
    }
    
    public function getWeight()
    {
    	return $weight;
    }
    
    public function getMaxLength()
    {
    	return $maxLength;
    }
}
