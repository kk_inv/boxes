<?php 


class Product
{
    private $id;
    private $package;
    
    public function __construct($id, $package)
    {
        $this->id = $id;
        $this->package = $package;
    }
    
    public function getId()
    {
        return $this->id;
    }
}
